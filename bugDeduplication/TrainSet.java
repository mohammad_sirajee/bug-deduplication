import java.util.*;


public class TrainSet {

	private Vector<TSI> TS = new Vector<TSI>();
	
	public TrainSet(Vector<Document> documents, int N){
		
		Map<Integer,Vector<Integer>> buckets = new Buckets(documents).getBuckets();
		Map<Integer,Document> idDocMap = new Corpus(documents).getIdDocMap();
		constructTrainSet(documents,buckets,idDocMap,N);
	}
	
	private void constructTrainSet(Vector<Document> documents, Map<Integer,Vector<Integer>> buckets, Map<Integer,Document> idDocMap, int N){
		
		System.out.println("number of buckets: "+buckets.size());
		
		List<Integer> bucketKeys = new ArrayList<Integer>(buckets.keySet());
		
		for(int bucketIndex=0; bucketIndex<bucketKeys.size(); bucketIndex++){
			
			Vector<Integer> R = new Vector<Integer>();
			R.add(bucketKeys.get(bucketIndex));
			if(buckets.get(bucketKeys.get(bucketIndex)) != null)
				R.addAll(buckets.get(bucketKeys.get(bucketIndex)));
			
			for(int qIndex=0;qIndex<R.size();qIndex++){
				for(int relIndex=0; relIndex!=qIndex && relIndex<R.size();relIndex++){
					for(int i=1;i<=N;i++){
						
						Document irr = idDocMap.get(getRandomIrrelevantDocumentID(bucketIndex,buckets));
						TS.add(new TSI(idDocMap.get(R.get(qIndex)),idDocMap.get(R.get(relIndex)),irr));
						
					}
				}
			}
		}
	}
	
	public Vector<TSI> getTS() {
		return TS;
	}
	
	public int getRandom(int min, int max)
	{
	    return (int) (Math.random() * (max - min + 1) ) + min;
	}
	
	private int getRandomIrrelevantDocumentID(int excludingBucketIndex, Map<Integer,Vector<Integer>> buckets){
		
		List<Integer> bucketKeys = new ArrayList<Integer>(buckets.keySet());
		int randomBucketIndex = getRandom(0, buckets.size()-1);
		while(randomBucketIndex == excludingBucketIndex){
			randomBucketIndex = getRandom(0, buckets.size()-1);
		}
		Vector<Integer> randomBucket = new Vector<Integer>();
		randomBucket.add(bucketKeys.get(randomBucketIndex));
		if(buckets.get(bucketKeys.get(randomBucketIndex))!=null)
			randomBucket.addAll(buckets.get(bucketKeys.get(randomBucketIndex)));
		
		int randomDocIndex = getRandom(0, randomBucket.size()-1);
		return randomBucket.get(randomDocIndex);
	}
	
}