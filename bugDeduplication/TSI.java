
public class TSI {

	private Document query;
	private Document rel;
	private Document irr;
	
	public TSI(Document query, Document rel, Document irr){
		this.query = query;
		this.rel = rel;
		this.irr = irr;
	}
	
	public Document getQuery() {
		return query;
	}
	public void setQuery(Document query) {
		this.query = query;
	}
	public Document getRel() {
		return rel;
	}
	public void setRel(Document rel) {
		this.rel = rel;
	}
	public Document getIrr() {
		return irr;
	}
	public void setIrr(Document irr) {
		this.irr = irr;
	}
	
}
