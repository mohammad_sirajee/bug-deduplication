import java.awt.image.ReplicateScaleFilter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.omg.CORBA.FREE_MEM;


public class Validate {

	Corpus corps;
	Buckets buckets;
	Map<Integer,Document> IdDocMap = new HashMap<Integer,Document>();
	REP rep;
	Map<String,Double> freeVariables = new HashMap<String,Double>(); /*= new HashMap<String, Double>(){{
		put("w1",0.9);
		put("w2",0.2);
		put("w3",2.0);
		put("w4",0.0);
		put("w5",0.7);
		put("w6",0.0);
		put("w7",0.0);
		put("wsumm_uni",3.0);
		put("wdesc_uni",1.0);
		put("bsumm_uni",0.5);
		put("bdesc_uni",1.0);
		put("k1_uni",2.0);
		put("k3_uni",0.0);
		put("wsumm_bi",3.0);
		put("wdesc_bi",1.0);
		put("bsumm_bi",0.5);
		put("bdesc_bi",1.0);
		put("k1_bi",2.0);
		put("k3_bi",0.0);
	}};*/
	boolean chronological;
	
	public Validate(Corpus corp, Map<String, Double> inputFreeVars,boolean stemming, boolean reweighting, String importantWordsFile, boolean chronological){

		chronological = chronological;
		freeVariables = inputFreeVars;
		corps = corp;
		buckets = new Buckets(corp.getDocuments());
		IdDocMap = corp.getIdDocMap();
		rep = new REP(corp,stemming,reweighting,importantWordsFile);
	}
	
	public void insertNewDoc(Document doc){
		
		corps.insertDocument(doc);
		IdDocMap = corps.getIdDocMap();
		rep.update(corps);
		buckets.insert(doc);
	}

	public Vector<SortableDoc> get_sorted_rep_list(Document doc){

		Vector<SortableDoc> rep_list = new Vector<SortableDoc>();
		Vector<Document> duplicatesOfBucket;

		for(Integer masterID:buckets.getBuckets().keySet()){

			duplicatesOfBucket = new Vector<Document>();

			if(buckets.getBuckets().get(masterID)!=null){
				for(int duplicateOfBucketId:buckets.getBuckets().get(masterID)){
					duplicatesOfBucket.add(IdDocMap.get(duplicateOfBucketId));
				}
			}
			
			SortableDoc current_master = getMaxREPAndMaster(doc,IdDocMap.get(masterID),duplicatesOfBucket);
			rep_list.add(current_master);
		}

		Collections.sort(rep_list);

		/*System.out.println("****************************************");
		System.out.println("duplicate doc id: "+doc.getBugID());
		System.out.println("correct master id: " +doc.getMergeID());
		for(SortableDoc sd:rep_list){
			System.out.println("master id: "+sd.getId()+" rep value: "+sd.getRep());
		}*/

		return rep_list;
	}

	private SortableDoc getMaxREPAndMaster(Document duplicateDoc, Document masterOfBucket, Vector<Document> duplicatesInBucket){

		double max = rep.getREP(masterOfBucket, duplicateDoc, freeVariables);

		for(Document currentDuplicateInBucket:duplicatesInBucket){
			if(currentDuplicateInBucket.getBugID()!=duplicateDoc.getBugID())
				max = Math.max(max, rep.getREP(currentDuplicateInBucket, duplicateDoc, freeVariables));
		}

		SortableDoc resultSD = new SortableDoc(masterOfBucket.getBugID(),max);

		return resultSD;
	}
	
	public Map<Document,Vector<SortableDoc>> traverseTestSet(Vector<Document> testSet){
		
		int counter = 0;
		Map sorted_duplicate_masters_map = new HashMap<Document,Vector<SortableDoc>>();
		for(Document doc:testSet){
			if(doc.getStatus().equals("Duplicate")){
				Vector<SortableDoc> sorted_list = get_sorted_rep_list(doc);
				sorted_duplicate_masters_map.put(doc, sorted_list);
				/*System.out.println("******************************");
				System.out.println("Duplicate number: " +counter ++);
				System.out.println("correct master: " +doc.getMasterID());
				for(int i=0;i<20;i++){
					System.out.println("ID: "+sorted_list.get(i).getId()+" Status: "+IdDocMap.get(sorted_list.get(i).getId()).getStatus()
				+" Rep: "+sorted_list.get(i).getRep());
				}
				System.out.println("-------------------------------");*/
			}
			insertNewDoc(doc);
			//System.out.println("corpus size: "+ corps.getDocuments().size()+"bucket numbers: "+ buckets.getBuckets().size()+"IddocMap size: "+ IdDocMap.size());
		}
		return sorted_duplicate_masters_map;
	}

	public Map<Document,Vector<SortableDoc>> get_sorted_duplicate_masters_map(){
		
		Map result = new HashMap<Document,Vector<SortableDoc>>();
		for(Document doc: corps.getDocuments()){
			//System.out.println(duplicateDoc.getBugID());
			if(doc.getStatus().equals("Duplicate"))
				result.put(doc, get_sorted_rep_list(doc));
		}
		return result;
	}

	public double recall(Map<Document,Vector<SortableDoc>> duplicate_bugs_masters_map, int k){

		int Ndetected = 0;
		int Ntotal = duplicate_bugs_masters_map.size();

		//System.out.println("number of duplicate bugs:"+duplicate_bugs_masters_map.size());
		for(Document doc:duplicate_bugs_masters_map.keySet()){
			//System.out.println("current duplicate bug:"+(++duplicateCounter));
			Vector<SortableDoc> rep_list = duplicate_bugs_masters_map.get(doc);
			for(int i=0;i<k;i++){
				if(rep_list.get(i).getId() == doc.getMasterID()){
					Ndetected++;
					break;
				}
			}
		}
		return (double)Ndetected/Ntotal;
	}

	public double MAP(Map<Document,Vector<SortableDoc>> duplicate_masters_map){

		Vector<Integer> master_indexes = new Vector<Integer>();
		double result = 0;

		for(Document doc:duplicate_masters_map.keySet()){

			Vector<SortableDoc> rep_list = duplicate_masters_map.get(doc);

			for(int i=0;i<rep_list.size();i++){
				if(rep_list.get(i).getId() == doc.getMasterID()){
					master_indexes.add(i+1);
					System.out.println("duplicate: "+doc.getDescription());
					System.out.println("master: "+ (IdDocMap.get(rep_list.get(i).getId())).getDescription());
					System.out.println("REP: "+rep_list.get(i).getRep());
					System.out.println("Best REP: "+rep_list.get(0).getRep());
					System.out.println("********************************");
					break;
				}
			}
		}

		for(int index:master_indexes){

			result += (double)1/index;
		}

		result = (double)result/master_indexes.size();
		return result;
	}

	public void writeToFile(String important_words,int k, String phrase, double value, String fileName){

		try{
			// Create file 
			FileWriter fstream = new FileWriter(fileName);
			BufferedWriter out = new BufferedWriter(fstream);

			if(phrase.equals("map"))
				out.append(important_words+"\n"+"map: "+value+"\n**************************\n");

			if(phrase.equals("recall"))
				out.append(important_words+"\n"+"k = "+k+"\n"+"recall: "+value+"\n**************************\n");

			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
    public static void warn(String warning) {
	System.err.println("WARNING: " + warning );
    }		
	public static void main(String[] args) {

		Preprocessing prep = new Preprocessing();
		AndroidXmlParser AXP = new AndroidXmlParser();
		/*Integer[] arr = {37520,37427,37198,37018,36995,36893,36870,36791,36747,
				36689,36680,36654,36644,34880,33788,37197,37019,36996,36987,36979,
				28557,26147,25712,25230,24811,23737,23171,22439,22003,21382};
		Vector<Integer> unitTestIds = new Vector<Integer>(Arrays.asList(arr));*/

		warn("get bugs");
		Vector<Document> corpus = AXP.getBugs();
		warn("sort 1");
		Collections.sort(corpus);
		warn("process");
		corpus = prep.process(corpus,false);
		warn("sort 2");
		Collections.sort(corpus);
		
		System.out.println(corpus.size());
		System.out.println(corpus.get(0).getOpenDate());
		System.out.println(corpus.get(corpus.size()-1).getOpenDate());

		warn("Count Duplicates");		
		int counter = 0;
		for(Document doc: corpus){
			if(doc.getStatus().equals("Duplicate"))
				counter++;
		}
		System.out.println("number of duplicates: " +counter);

		warn("Training Set");				
		Vector<Document> trainSet = new Vector<Document>(corpus.subList(0, 8200));
		Vector<Document> testSet = new Vector<Document>(corpus.subList(8200, corpus.size()));
		Corpus inputCorpus =  new Corpus(trainSet);
		
		TrainSet TS = new TrainSet(trainSet, 24);

		warn("Tune");				
		Tuning tuning = new Tuning(inputCorpus,false,false,"");
		Map<String,Double> freeVariables = tuning.tuningParametersInREP(TS.getTS());
		
		
		warn("Validate");				
		Validate validate = new Validate(inputCorpus,freeVariables,false,true,"NFR words.txt",true);
		Map<Document,Vector<SortableDoc>> duplicate_masters_map = validate.traverseTestSet(testSet);

		validate.writeToFile("witht_NFR_words", 0, "map", validate.MAP(duplicate_masters_map), "witht_NFR_words_MAP.txt");
		for(int k=1;k<21;k++){
			validate.writeToFile("with NFR words", k,"recall",validate.recall(duplicate_masters_map, k),
					"with_NFR_words_recall_k"+k+".txt");
		}
		//System.out.println(validate.recall(duplicate_masters_map, 10));
		//System.out.println(validate.MAP(duplicate_masters_map));
	}
}
