import java.util.*;

public class ExtendedREP extends REP {
    
    public ExtendedREP(Corpus corps, boolean stemming, boolean reweighting, String importantWordsFile){
        super(corps, stemming, reweighting, importantWordsFile);
    }

    public double[] getExtendedFeatures(Document doc, Document query, Map<String, Double> freeVariables, Vector<Document> dictionaries) {
        double [] features = this.getFeatures(doc, query, freeVariables);
        double [] intersectionsDoc   = new double[dictionaries.size()];
        double [] intersectionsQuery = new double[dictionaries.size()];
        for (int i = 0 ; i < dictionaries.size(); i++) {
            intersectionsDoc[i]   = bmf_unigram.calculateBM25F( doc , dictionaries.get(i), freeVariables);
            intersectionsQuery[i] = bmf_unigram.calculateBM25F( query , dictionaries.get(i), freeVariables);            
        }
        double dist = euclideanDistance( intersectionsDoc,  intersectionsQuery );
        int len = features.length;
        len += intersectionsDoc.length  + intersectionsQuery.length;
        len += 1; //distance
        double [] extended = new double[len];
        for (int i = 0; i < features.length; i++) {
            extended[i] = features[i];
        }
        int offset = features.length;
        for (int i = 0; i < intersectionsDoc.length; i++) {
            extended[offset + i] = intersectionsDoc[i];
        }
        offset = features.length + intersectionsDoc.length;
        for (int i = 0; i < intersectionsQuery.length; i++) {
            extended[offset + i] = intersectionsQuery[i];
        }
        offset = features.length + intersectionsDoc.length + intersectionsQuery.length;
        extended[offset] = dist;
        return extended;       
    }
    public static double euclideanDistance( double [] a, double [] b) {
        double sum = 0.0;
        for (int i = 0 ; i < a.length ; i++ ) {
            double sq = a[i] - b[i];
            sq *= sq;
            sum += sq;
        }
        return Math.sqrt( sum );
    }
}
