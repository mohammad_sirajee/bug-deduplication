import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.Utilities;


public class Document implements Comparable<Document>{

	private String description;
	private String title;
	private String status;
	private int bugID;
	public double[] weights;
	private String component;
	private String priority;
	private String type;
	private int stars;
	private String version;
	private int mergeID;
	private int masterID;
	private Date openDate;
	private String dateForamt = "yyyy-MM-dd'T'HH:mm:ss'.000Z'";
	private Date closeDate;

	public Document(String inputDesc, String inputTitle, int inputId, String inputStatus,
			int input_stars, String input_component, String input_type , String input_priority,
			String input_version,int merge_id, String openDate, String closeDate){
		setMergeID(merge_id);
		setStars(input_stars);
		setComponent(input_component);
		setType(input_type);
		setPriority(input_priority);
		setVersion(input_version);
		setDescription(inputDesc);
		setTitle(inputTitle);
		setBugID(inputId);
		setStatus(inputStatus);
		setOpenDate(openDate);
		setCloseDate(closeDate);
	}

	public void setOpenDate(String openDate) {

		if(openDate.equals(""))
			this.openDate = new Date();
		else
			this.openDate = getDate(openDate);
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setCloseDate(String closeDate) {
		if(closeDate.equals(""))
			this.closeDate = new Date();
		else
			this.closeDate = getDate(closeDate);
	}

	public Date getCloseDate() {
		return closeDate;
	}

	private Date getDate(String inputDate){

		DateFormat sdf = new SimpleDateFormat(this.dateForamt);
		Date date = new Date();
		try {
			date = sdf.parse(inputDate);
			String GMTTime = date.toGMTString();
			date = new Date(GMTTime);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description.toLowerCase();
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title.toLowerCase();
	}
	public int getBugID() {
		return bugID;
	}
	public void setBugID(int bugID) {
		this.bugID = bugID;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getMergeID() {
		return mergeID;
	}

	public void setMergeID(int mergeID) {
		this.mergeID = mergeID;
	}

	public int getMasterID() {
		return masterID;
	}

	public void setMasterID(int masterID) {
		this.masterID = masterID;
	}


	@Override
	public int compareTo(Document doc2) {
		// TODO Auto-generated method stub
		if(this.getOpenDate().after(doc2.getOpenDate()))
			return 1;
		else if(this.getOpenDate().before(doc2.getOpenDate()))
			return -1;

		return 0;
	}
}
