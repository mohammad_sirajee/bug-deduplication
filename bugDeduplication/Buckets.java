import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class Buckets {

	private Map<Integer,Vector<Integer>> buckets = new HashMap<Integer, Vector<Integer>>();

	public Buckets(Vector<Document> corpus){

		for(Document doc: corpus)
			insert(doc);
	}

	public void insert(Document doc){
		
		if(doc.getStatus().equals("Duplicate")){
			buckets.get(doc.getMasterID()).add(doc.getBugID());
		}
		
		else{
			buckets.put(doc.getBugID(), new Vector<Integer>());	
		}
	}
	
	public Map<Integer, Vector<Integer>> getBuckets() {
		
		return buckets;
	}

}
