import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class Corpus{

	private Vector<Document> documents = new Vector<Document>();
	private Map<String,Integer> priorityMap = new HashMap<String,Integer>(){{
		put("Small",1);
		put("Medium",2);
		put("High",3);
		put("Critical",4);
		put("Blocker",5);
	}};
	private Map<Integer,Document> idDocMap = new HashMap<Integer,Document>();
	private double avgTitleLength;
	private double avgDescriptionLength;

	public Corpus(Vector<Document> inputCorpus){
		documents = inputCorpus;
		createIdDocMap(documents);
		updateAvgDescriptionLength(0);
		updateAvgTitleLength(0);
	}
	
	public void insertDocument(Document newDoc){
		
		documents.add(newDoc);
		idDocMap.put(newDoc.getBugID(), newDoc);
		updateAvgDescriptionLength(documents.size()-1);
		updateAvgTitleLength(documents.size()-1);
	}

	private void createIdDocMap(Vector<Document> corpus){
		for(Document doc: corpus){
			idDocMap.put(doc.getBugID(), doc);
		}
	}
	
	public Map<Integer, Document> getIdDocMap() {
		return idDocMap;
	}
	
	public double getAvgTitleLength(){

		return this.avgTitleLength;

	}

	public double getAvgDescriptionLength(){

		return this.avgDescriptionLength;

	}
	
	public double getVersionNumber(String version){

		if(version.equals(""))
			return -1;

		if(version.startsWith("r"))
			return Double.parseDouble(version.substring(1));

		if(version.equals("OpenSource"))
			return 0;

		return Double.parseDouble(version);
	}

	public int getPriorityNumber(String priority){

		if(priority.equals(""))
			return -1;

		return priorityMap.get(priority);
	}

	public void updateAvgTitleLength(int previousSize) {
		
		double sumLength = getAvgTitleLength()*previousSize;
		for(int i=previousSize;i<documents.size();i++){
			sumLength += documents.get(i).getTitle().length();
		}

		this.avgTitleLength = ((double)(sumLength)/documents.size());
	}

	public void updateAvgDescriptionLength(int previousSize) {
		
		double sumLength = getAvgDescriptionLength()*previousSize;
		for(int i=previousSize;i<documents.size();i++){
			sumLength += documents.get(i).getDescription().length();
		}

		this.avgDescriptionLength = ((double)(sumLength)/documents.size());
	}

	public Vector<Document> getDocuments() {
		return documents;
	}
	
	public void setDocuments(Vector<Document> documents) {
		this.documents = documents;
	}
}
