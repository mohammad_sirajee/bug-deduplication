import java.lang.reflect.Array;
import java.util.ArrayList;


public class SortableDoc implements Comparable<SortableDoc>{

	private int id;
	private double rep;
	
	public SortableDoc(int id, double rep){
		
		this.id = id;
		this.rep = rep;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getRep() {
		return rep;
	}

	public void setRep(double rep) {
		this.rep = rep;
	}

	@Override
	public int compareTo(SortableDoc o) {
		
		// TODO Auto-generated method stub
		if(getRep()>o.getRep())
			return -1;
		if(getRep()<o.getRep())
			return 1;
		return 0;
	}
	
}
